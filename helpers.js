const random_source = d3.randomLcg();
const random = d3.randomUniform.source(random_source);
function getRandom() {
  // const random = d3.randomUniform.source(random_source);
  return random(0, 1)();
}

function getRandomFloat(min, max) {
  return getRandom() * (max - min) + min;
}

function s_curve(t) {
  return t * t * (3.0 - 2.0 * t);
}

function lerp(t, a, b) {
  return a + t * (b-a);
}

function createStep(text) {
  var foo = document.createElement('li');
  foo.innerHTML = text;
  return foo;
}

function RandomNumber(y, color, updateFn) {
  this.y = y;
  this.color = color;
  this.updateFn = updateFn;
}

RandomNumber.prototype = {
  update(y) {
    this.y = y;
    this.updateFn(y);
    
  }
};

function dragRandomNumberHandle(yAxis) {
  // function dragstarted() {
  //   d3.select(this).attr("stroke", "black");
  // }

  function dragged(event, d) {
    let svgSpaceY = event.y;
    let axisSpaceY = yAxis.invert(svgSpaceY);

    if(axisSpaceY > 1.0) {
      axisSpaceY = 1.0;
    }

    if(axisSpaceY < -1.0) {
      axisSpaceY = -1.0;
    }

    d.rand.update(axisSpaceY);
    // console.log(d);

    // callback(axisSpaceY);
    d3.select(this)
      .attr("cy", d.y = event.y);
  }

  // function dragended() {
  //   d3.select(this).attr("stroke", null);
  // }

  return d3.drag()
      //.on("start", dragstarted)
      .on("drag", dragged)
      //.on("end", dragended);
}