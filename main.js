const steps = document.querySelector("#steps");

function noise1(x, rand0, rand1) {
  // const t = x + env.N;
  // // The index of the current random number selected
  // const bx0 = Math.floor(t) & env.BM;
  // // The index of the next random number
  // const bx1 = (bx0 + 1) & env.BM;
  // 
  const rx0 = x - Math.floor(x); // r0 = decimal part of t
  const rx1 = rx0 - 1.0;

  let sx = s_curve(rx0);
    
  const u = rx0 * rand0;
  const v = rx1 * rand1;
  const result = lerp(sx, u, v);

  return {
    x, rx0, rx1, sx, u, v, result 
  }
}

const width = 1300;
const resolution = 100;

const xDomain = [0.8, 3.2];
const yDomain = [-1.1, 1.1];
  
const height = 0.3 * width;
const margin = {top: 10, right: 30, bottom: 30, left: 30},
  _width = width * 0.5 - margin.left - margin.right,
  _height = height - margin.top - margin.bottom;

// Initialize the canvas
var parent = d3.select("#my_dataviz")
.append("svg")
  //.attr("viewBox", [0, 0, _width, _height])
  .attr("width", _width + margin.left + margin.right)
  .attr("height", _height + margin.top + margin.bottom)
.append("g")
  .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

function setUpCanvas(drawAxes) {
  const svg = parent.append("g")
    .attr("transform", `translate(${margin.left},${margin.top})`);

  // Draw the X and Y-Axis
  const xAxis = d3.scaleLinear()
    //.domain([0, d3.max(data, (d) => d.x)])
    .domain(xDomain)
    .range([0, _width]);

  const yAxis = d3.scaleLinear()
    //.domain([0, d3.max(data, (d) => d.y)])
    .domain(yDomain)
    .range([_height, 0]);


  if(drawAxes) {
    svg.append("g")
      .attr("transform", `translate(0, ${_height})`)
      .call(d3.axisBottom(xAxis).tickValues([1, 1.5, 2, 2.5, 3]));

    svg.append("g")
      .call(d3.axisLeft(yAxis));
  }



  // function clicked(event, d) {
  //   if (event.defaultPrevented) return;
  //   d3.select(this).transition()
  //         .attr("fill", "black")
  //         .attr("r", 5 * 2)
  //       .transition()
  //         .attr("r", 5)
  //         .attr("fill", d3.schemeCategory10[d.index % 10]);
  // }
  
  
  let foodrag = dragRandomNumberHandle(yAxis);



  function drawInterval(x0, x1, rand0, rand1) {

    

    // // Select our random numbers
    // const y1 = getRandomFloat(-1.0, 1.0);
    // const y2 = getRandomFloat(-1.0, 1.0);
    const points = [//{x: 1, color: "black", y: 0},
      { x: xAxis(x0), color: rand0.color, y: yAxis(rand0.y * 1.0), rand: rand0 },
      { x: xAxis(x1), color: rand1.color, y: yAxis(rand1.y * -1.0), rand: rand1 },
      
    ];



    const indices = d3.range(x0, x1, 1.0/resolution);
    const allData = indices.map((x) => {
      return noise1(x, rand0.y, rand1.y);
    })

    const allGroups = ["u", "v", "result"];

    let data = {};

    allGroups.forEach((name) => {
      data[name] = [];
    });

    allData.forEach((d) => {
      allGroups.forEach((name) => {
        data[name].push({x: xAxis(d.x), y: yAxis(d[name])});
      });
    });

    // Set up the gradient
    const svgDefs = svg.append('defs');
    const mainGradient = svgDefs.append('linearGradient')
                    .attr('id', 'mainGradient');


    const gradientInterpol = d3.interpolateLab("blue", "orange");
    d3.range(0, 11, 1).forEach((idx) => {
      let i = idx / 10.0;
      let s = s_curve(i);
      mainGradient.append('stop')
        .attr('offset', i)
        .attr('stop-color', gradientInterpol(s))
        //.attr('stop-color', `rgb(0, 0, ${i*255})`);
    });

    const gradient = svg.append('rect');
    // gradient.classed('filled', true)
    //   .attr('x', x(x0))
    //   .attr('y', y(1.0))
    //   .attr('width', x(x1) - x(x0))
    //   .attr('height', y(-1.0) - y(1.0))
    //   .style('opacity', 0.5);

    // Draw the lines
    const line = d3.line()
          .x(function(d) { return d.x })
          .y(function(d) { return d.y });

    // svg.append("path")
    //   .datum(data.v)
    //   .attr("d", (d) => line(d) )
    //     .attr("stroke", "orange")
    //     .style("stroke-width", 2)
    //     .style("fill", "none")

    svg.selectAll('.randline').remove();

    svg
        .append("line")
        .attr("class", "randline")
        .attr("stroke", rand1.color)
        .attr("x1", xAxis(x0))
        .attr("x2", xAxis(x1))
        .attr("y1", yAxis(rand1.y*-1))
        .attr("y2", yAxis(rand1.y*-1))
        .style("stroke-width", 2)
        .style("fill", "none")

    svg.append("line")
        .attr("class", "randline")
        .attr("stroke", rand0.color)
        .attr("x1", xAxis(x0))
        .attr("x2", xAxis(x1))
        .attr("y1", yAxis(rand0.y))
        .attr("y2", yAxis(rand0.y))
        .style("stroke-width", 2)
        .style("fill", "none")

    // svg.append("path")
    //   .datum(data.u)
    //   .attr("d", (d) => line(d) )
    //     .attr("stroke", "blue")
    //     .style("stroke-width", 2)
    //     .style("fill", "none");


    svg.selectAll(".pathresult").remove();

    const path_result = svg.append("path");
    path_result.attr("class", "pathresult");
    path_result
      .datum(data.result)
      .attr("d", (d) => line(d) )
        .attr("stroke", "black")
        .style("stroke-width", 4)
        .style("fill", "none")
        .style("opacity", 0.5)
        .attr("stroke-dasharray", function() {
          return this.getTotalLength();
        })
        .attr("stroke-dashoffset", function() {
          return 0;
          //return this.getTotalLength();
        });



    // svg.selectAll("myLines")
    //       .data(data)
    //       .enter()
    //       .append("path")
    //         .attr("d", function(d){ return line(d.values) } )
    //         //.attr("stroke", function(d){ return myColor(d.name) })
    //         .attr("stroke", "black")
    //         .style("stroke-width", 4)
    //         .style("fill", "none")



  

    // Draw the random number dots
    svg.selectAll('circle')
        .data(points, d => d.y)
        .join("circle")  
          .attr("cx", (d) => d.x)
          .attr("cy", (d) => d.y)
          .attr("r", 10)
          .style("fill", (d) => d.color)
          .call(foodrag)
          //.on("click", clicked);
            
          //.style("opacity", 0.5)
        





    // function htmlToElement(html) {
    //   var template = document.createElement('template');
    //   template.innerHTML = "<div>" + html + "</div>";
    //   return template.content.childNodes;
    // }

    /// Transition setup
    let current_step = 1;
    const transitions = [
      () => {
        // steps.appendChild(createStep("We choose two random numbers"));
        // circles
        // .selectAll("circle")
        // .transition()
        // .duration(500)
        // .attr("r", 5);
      },

      () => {
        steps.appendChild(createStep("wat"));
        gradient.transition().duration(500).style("opacity", 0.5);
      },

      () => {
        const tween = (x) => {
          let start = path_result.attr("stroke-dashoffset");
          return d3.interpolateNumber(start, 0)
        }
        path_result
        .transition()
                .duration(2000)
                .attrTween("stroke-dashoffset", tween);
      }
    ];

    function next() {
      if(current_step <= transitions.length) {
        transitions[current_step-1]();
        current_step+=1;
      }
    }

    transitions.forEach((f) => f());
  }

  return drawInterval;
}




// const rand0 = getRandomFloat(-1.0, 1.0);
// const rand1 = getRandomFloat(-1.0, 1.0);
// const rand2 = getRandomFloat(-1.0, 1.0);

const drawInterval1 = setUpCanvas(true);
const drawInterval2 = setUpCanvas(false);

const rand0 = new RandomNumber(0.56, "blue", randchanged);
const rand1 = new RandomNumber(0.08, "orange", randchanged);
const rand2 = new RandomNumber(0.42, "green", randchanged);




// document.getElementById("rand0").value = rand0;
// document.getElementById("rand1").value = rand1;
// document.getElementById("rand2").value = rand2;

randchanged();

function update(rand0, rand1, rand2) {
  drawInterval1(1.0, 2.0, rand0, rand1);
  drawInterval2(2.0, 3.0, rand1, rand2);
}

function randchanged() {
  // let rand0 = document.getElementById("rand0").value;
  // let rand1 = document.getElementById("rand1").value;
  // let rand2 = document.getElementById("rand2").value;
  update(rand0, rand1, rand2);
}




